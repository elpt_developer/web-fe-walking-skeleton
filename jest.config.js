module.exports = {
    "verbose": true,
    "testURL": "http://localhost/",
    "automock": false,
    "transform": {
        "^.+\\.jsx?$": "babel-jest",
        "^.+\\.*ss?$": "<rootDir>/node_modules/jest-css-modules"
    },
    "roots": [
        "<rootDir>/src"
    ],
    "moduleDirectories": [
        "node_modules",
        "src"
    ],
    "moduleFileExtensions": [
        "js",
        "jsx",
        "json"
    ],
    "moduleNameMapper": {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/config/jest/fileMock.js",
        "\\.(css|less)$": "identity-obj-proxy"
    },
    "collectCoverage": true,
    "collectCoverageFrom": [
        "src/**/*.{js,jsx}",
        "!src/registerServiceWorker.js"
    ],
    "testPathIgnorePatterns" : ["<rootDir>/src/registerServiceWorker.js" ],
    /*
    "coverageReporters": [
        "json", "lcov", "text",
        "cobertura",
        "html"
    ],*/
    "coverageDirectory": "target/coverage/",
    /* Enable the following section to set global threshold for Test Passing*/
    "coverageThreshold": {
        "global": {
            "statements": 90,
            "branches": 90,
            "functions": 90,
            "lines": 90
        }
    }
};
