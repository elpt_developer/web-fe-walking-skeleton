import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

import Logo from './Logo'

it('renders without crashing', () => {
    const props = {}
    const wrapper = Enzyme.mount(<Logo {...props}/>)
    expect(wrapper!=undefined).toBe(true)
});