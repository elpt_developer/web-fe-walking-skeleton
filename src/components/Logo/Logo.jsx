import React from 'react'
import styles from './Logo.scss'
const Logo = (props) => {
    return <a className={styles["Logo-link"]} href="https://everskill.de" target="_blank" rel="noopener noreferrer">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 396.85 104.88" fill="#333">
            <polygon className="cls-1" points="89.61 14.97 90.91 12.11 92.68 8.19 83.24 5.77 86.25 10.12 89.61 14.97"></polygon>
            <polygon className="cls-1" points="89.2 15.4 82.75 6.08 82.74 6.1 81.19 17.93 89.2 15.4"></polygon>
            <polygon className="cls-1" points="47.05 31.55 55.45 14.5 43.94 14.5 45.39 22.48 47.05 31.55"></polygon>
            <polygon className="cls-1" points="95.08 14.34 97.58 13.91 96.78 12.94 93.16 8.54 90.16 15.17 95.08 14.34"></polygon>
            <polygon className="cls-1" points="81.83 8.63 82.21 5.68 72.03 8.33 80.64 17.69 81.83 8.63"></polygon>
            <polygon className="cls-1" points="62.68 21.96 80.29 18.17 71.52 8.65 62.92 21.59 62.68 21.96"></polygon>
            <polygon className="cls-1" points="47.41 32.13 58.9 31.04 58.52 29.11 55.79 15.11 51.92 22.97 47.41 32.13"></polygon>
            <polygon className="cls-1" points="96.69 6.81 97.95 6.5 93.22 4.73 93.36 7.63 96.69 6.81"></polygon>
            <polygon className="cls-1" points="70.71 8.82 63.48 11.88 62.46 21.23 70.71 8.82"></polygon>
            <polygon className="cls-1" points="62.87 12.11 56.39 14.35 61.83 21.62 62.87 12.11"></polygon>
            <polygon className="cls-1" points="92.63 4.63 84.24 5.42 92.77 7.61 92.63 4.63"></polygon>
            <polygon className="cls-1" points="102.74 17.94 97.89 20.36 105.79 25.92 102.74 17.94"></polygon>
            <polygon className="cls-1" points="110.74 32.32 111 25.45 111.05 24.19 109.6 25.09 106.83 26.8 108.12 28.63 110.74 32.32"></polygon>
            <polygon className="cls-1" points="109.27 24.6 110.83 23.64 103.52 18.35 106.56 26.28 109.27 24.6"></polygon>
            <polygon className="cls-1" points="97.78 14.47 93.1 15.27 90.47 15.71 97.08 19.83 97.78 14.47"></polygon>
            <polygon className="cls-1" points="98.35 14.64 97.68 19.82 102.33 17.49 98.35 14.64"></polygon>
            <polygon className="cls-1" points="109.02 16.34 104.58 12.15 103.29 17.18 109.02 16.34"></polygon>
            <polygon className="cls-1" points="103.89 11.5 99.17 7.16 99.14 7.43 98.47 13.66 103.89 11.5"></polygon>
            <polygon className="cls-1" points="90.9 25.75 92.22 24.54 96.83 20.37 90.01 16.11 90.39 20.32 90.9 25.75"></polygon>
            <polygon className="cls-1" points="110.89 22.95 109.43 16.87 103.65 17.72 110.89 22.95"></polygon>
            <polygon className="cls-1" points="93.61 8.17 95.21 10.12 97.91 13.39 97.98 12.71 98.6 6.94 97.09 7.31 93.61 8.17"></polygon>
            <polygon className="cls-1" points="98.73 14.19 99.96 15.07 102.72 17.05 103.14 15.44 104 12.08 102.09 12.84 98.73 14.19"></polygon>
            <polygon className="cls-1" points="18.23 30.16 18.25 30.15 31.01 26.3 22.96 25.28 13.8 24.12 16.72 28.1 18.23 30.16"></polygon>
            <polygon className="cls-1" points="32.4 25.78 34.34 23.7 42.81 14.6 22.32 17.37 30.01 23.78 32.4 25.78"></polygon>
            <polygon className="cls-1" points="21.62 17.55 13.9 23.54 31.47 25.76 21.62 17.55"></polygon>
            <polygon className="cls-1" points="37.45 28.04 46.53 31.99 46.48 31.7 43.4 14.83 32.92 26.07 37.45 28.04"></polygon>
            <polygon className="cls-1" points="46.97 33.14 42 45.76 56.32 48.79 46.97 33.14"></polygon>
            <polygon className="cls-1" points="6.6 64.36 6.47 62.4 5.95 63.49 6.6 64.36"></polygon>
            <polygon className="cls-1" points="14.88 48.06 6.05 34.62 6.7 46.37 14.88 48.06"></polygon>
            <polygon className="cls-1" points="44.37 38.15 46.39 33.02 27.24 41.25 41.43 45.62 44.37 38.15"></polygon>
            <polygon className="cls-1" points="13.16 24.23 6.43 33.07 17.62 30.32 13.16 24.23"></polygon>
            <polygon className="cls-1" points="17.45 33.08 17.77 30.88 6.16 33.74 15.33 47.69 17.45 33.08"></polygon>
            <polygon className="cls-1" points="34.57 37.46 46.18 32.47 43.88 31.47 32.58 26.56 30.66 31.4 26.93 40.75 34.57 37.46"></polygon>
            <polygon className="cls-1" points="23.99 37.68 26.32 40.71 31.93 26.63 18.61 30.65 23.99 37.68"></polygon>
            <polygon className="cls-1" points="23.06 68.29 16.27 72.16 18.3 72.86 24.87 75.14 23.06 68.29"></polygon>
            <polygon className="cls-1" points="25.82 55.12 17.46 57.48 11.58 59.14 23.06 67.35 24.96 58.93 25.82 55.12"></polygon>
            <polygon className="cls-1" points="15.63 83.57 24.71 75.7 15.85 72.63 15.63 83.57"></polygon>
            <polygon className="cls-1" points="17.35 37.77 15.89 47.89 25.97 41.22 18.3 31.22 17.35 37.77"></polygon>
            <polygon className="cls-1" points="26.05 44.88 26.1 41.84 20.01 45.86 16.06 48.48 25.93 54.2 26.05 44.88"></polygon>
            <polygon className="cls-1" points="26.52 54.2 40.87 46.05 38.83 45.42 26.68 41.69 26.63 45.88 26.52 54.2"></polygon>
            <polygon className="cls-1" points="26.4 55.21 24.42 63.96 23.65 67.38 36.06 62.63 26.4 55.21"></polygon>
            <polygon className="cls-1" points="39.83 50.95 41.12 46.58 37.43 48.68 26.75 54.74 36.5 62.23 39.83 50.95"></polygon>
            <polygon className="cls-1" points="81.13 18.95 80.97 34.95 90.21 26.37 81.13 18.95"></polygon>
            <polygon className="cls-1" points="122.38 36.99 123.8 36.27 121.53 35.45 121.98 36.27 122.38 36.99"></polygon>
            <polygon className="cls-1" points="121.85 37.22 121.45 36.5 121.1 35.87 120.61 37.68 121.85 37.22"></polygon>
            <polygon className="cls-1" points="125.24 37.8 124.8 37.15 124.43 36.6 123.1 37.28 125.24 37.8"></polygon>
            <polygon className="cls-1" points="128.42 38.39 127.91 37.77 126.98 38.15 128.42 38.39"></polygon>
            <polygon className="cls-1" points="126.02 37.91 126.29 37.8 127.17 37.45 125.26 36.79 125.35 36.93 126.02 37.91"></polygon>
            <polygon className="cls-1" points="118.72 35.33 120.63 34.78 118.6 30.85 116.69 35.91 118.72 35.33"></polygon>
            <polygon className="cls-1" points="113.97 22.19 110.23 17.7 111.56 23.25 113.97 22.19"></polygon>
            <polygon className="cls-1" points="111.65 33.25 116.07 35.91 116.19 35.59 118.05 30.65 113.89 32.34 111.65 33.25"></polygon>
            <polygon className="cls-1" points="117.55 28.83 114.32 22.68 111.89 23.74 117.55 28.83"></polygon>
            <polygon className="cls-1" points="111.31 32.76 114.69 31.39 118.01 30.04 111.63 24.29 111.31 32.76"></polygon>
            <polygon className="cls-1" points="116.05 36.69 114.24 37.13 112.84 37.47 115.69 38.45 117.03 38.91 116.05 36.7 116.05 36.69"></polygon>
            <polygon className="cls-1" points="119.29 35.77 117.06 36.41 118.87 37.23 119.98 37.74 120.63 35.39 119.29 35.77"></polygon>
            <polygon className="cls-1" points="120.67 40.26 120.02 38.55 118.31 39.37 120.67 40.26"></polygon>
            <polygon className="cls-1" points="120.93 40.98 119.79 40.55 118.25 39.98 122.98 47.73 120.93 40.98"></polygon>
            <polygon className="cls-1" points="90.05 22.92 89.41 15.94 81.45 18.46 87.19 23.14 90.3 25.69 90.05 22.92"></polygon>
            <polygon className="cls-1" points="79.76 82.22 78.27 85.9 76.94 89.16 79.5 88.38 81.9 87.64 81.05 85.49 79.76 82.22"></polygon>
            <polygon className="cls-1" points="120.94 39.34 121.23 40.12 121.78 39.28 121.92 39.08 120.67 38.63 120.94 39.34"></polygon>
            <polygon className="cls-1" points="122.82 41.39 122.36 41.29 121.59 41.14 123.35 46.95 122.82 41.39"></polygon>
            <polygon className="cls-1" points="122.28 39.59 121.92 40.15 121.65 40.55 121.87 40.6 122.65 40.76 122.31 39.71 122.28 39.59"></polygon>
            <polygon className="cls-1" points="122 37.78 121.89 37.83 121.03 38.14 121.26 38.22 122.05 38.5 122 37.78"></polygon>
            <polygon className="cls-1" points="130.28 38.51 128.89 38.04 129.29 38.51 130.28 38.51"></polygon>
            <polygon className="cls-1" points="56.45 15.4 57.61 21.38 59.31 30.12 60.19 27.33 61.72 22.45 58.15 17.68 56.45 15.4"></polygon>
            <polygon className="cls-1" points="75.47 59.72 80.37 48.09 65.09 55.23 75.09 59.56 75.47 59.72"></polygon>
            <polygon className="cls-1" points="67.12 42.15 64.84 54.34 64.77 54.74 80.21 47.52 67.12 42.15"></polygon>
            <polygon className="cls-1" points="66.49 41.68 65.23 39.96 59.44 32.07 57.28 48.57 66.49 41.68"></polygon>
            <polygon className="cls-1" points="81.89 93.37 81.99 88.22 77.07 89.73 81.89 93.37"></polygon>
            <polygon className="cls-1" points="77.22 71.5 77.42 71.53 75.43 60.68 65.82 69.38 77.22 71.5"></polygon>
            <polygon className="cls-1" points="85.81 62.91 76 60.5 77.97 71.28 81.89 67.09 85.81 62.91"></polygon>
            <polygon className="cls-1" points="87.22 44.51 80.98 36.5 81.22 47.07 82.56 46.5 87.22 44.51"></polygon>
            <polygon className="cls-1" points="5.01 55.13 5.17 55.28 6.34 56.4 6.52 53.19 6.33 53.43 5.01 55.13"></polygon>
            <polygon className="cls-1" points="83.83 56.51 80.91 48.31 76.03 59.9 81.24 61.19 85.91 62.33 83.83 56.51"></polygon>
            <polygon className="cls-1" points="64.18 54.71 65.7 46.56 66.47 42.42 57.38 49.22 58.69 50.28 64.18 54.71"></polygon>
            <polygon className="cls-1" points="73.12 76.02 70.98 77.74 79.37 80.97 77.6 72.42 73.12 76.02"></polygon>
            <path className="cls-1" d="M80.38,35.88h0l-.09-.05-.09-.06,0,0a.43.43,0,0,1-.1-.09l-12-6.29,3.18,4.36,9.39,12.87Z"></path>
            <polygon className="cls-1" points="70.67 78.25 70.36 82.79 71.64 82.57 78.68 81.33 70.67 78.25"></polygon>
            <polygon className="cls-1" points="62.7 35.53 66.62 40.87 66.74 31.55 66.78 28.93 59.72 31.45 62.7 35.53"></polygon>
            <polygon className="cls-1" points="79.3 81.81 70.64 83.33 76.33 89.11 79.3 81.81"></polygon>
            <polygon className="cls-1" points="63.3 23.48 67.13 28.12 72.56 24.1 79.58 18.92 62.56 22.58 63.3 23.48"></polygon>
            <polygon className="cls-1" points="67.22 39.55 67.19 41.55 76.27 45.27 80.09 46.84 67.36 29.39 67.22 39.55"></polygon>
            <polygon className="cls-1" points="60.43 28.51 59.7 30.83 66.59 28.38 66 27.66 62.16 23 60.43 28.51"></polygon>
            <polygon className="cls-1" points="69.16 29.28 80.38 35.14 80.55 18.93 67.62 28.47 69.16 29.28"></polygon>
            <polygon className="cls-1" points="56.63 50.25 49.39 62.26 56.7 59.63 56.66 54.61 56.63 50.25"></polygon>
            <polygon className="cls-1" points="110.76 33.52 105.88 34.02 111.41 36.89 111.23 35.95 110.76 33.52"></polygon>
            <polygon className="cls-1" points="113.57 35.09 111.41 33.78 111.71 35.38 112.04 37.07 115.46 36.23 113.57 35.09"></polygon>
            <polygon className="cls-1" points="106.55 27.42 105.24 33.49 110.47 32.96 106.55 27.42"></polygon>
            <polygon className="cls-1" points="115.18 38.89 112.33 37.91 115.26 44.96 117.19 39.59 115.18 38.89"></polygon>
            <polygon className="cls-1" points="105.95 27.45 100.42 31.81 100.17 32 104.66 33.45 105.66 28.79 105.95 27.45"></polygon>
            <polygon className="cls-1" points="105.92 26.73 97.74 20.98 99.76 31.59 103.34 28.76 105.92 26.73"></polygon>
            <polygon className="cls-1" points="116.62 61.2 119.8 58.17 116.81 55 116.62 61.2"></polygon>
            <polygon className="cls-1" points="123.28 50.58 120.4 52.3 117.01 54.35 120.12 57.67 123.28 50.58"></polygon>
            <polygon className="cls-1" points="117.63 40.08 115.65 45.61 123.17 49.17 117.63 40.08"></polygon>
            <polygon className="cls-1" points="123.31 49.88 115.65 46.26 116.77 53.81 123.31 49.88"></polygon>
            <polygon className="cls-1" points="86.18 61.34 87.36 45.08 81.3 47.66 86.18 61.34"></polygon>
            <polygon className="cls-1" points="84.57 76.38 85.98 63.59 78.23 71.85 84.57 76.38"></polygon>
            <polygon className="cls-1" points="79.95 80.87 81.97 79.06 84.34 76.93 78.22 72.57 79.95 80.87"></polygon>
            <polygon className="cls-1" points="80.11 81.52 82.21 86.85 84.32 77.74 83.55 78.42 80.11 81.52"></polygon>
            <polygon className="cls-1" points="74.58 97.05 83.84 98.27 82.02 94.31 74.58 97.05"></polygon>
            <polygon className="cls-1" points="116.94 68.5 116.17 63.03 114.4 69.47 116.45 68.68 116.94 68.5"></polygon>
            <polygon className="cls-1" points="94.82 22.98 91.14 26.32 99.15 31.5 97.13 20.89 94.82 22.98"></polygon>
            <polygon className="cls-1" points="91.93 31.04 93.44 38.26 99.12 32.17 91.09 26.98 91.93 31.04"></polygon>
            <polygon className="cls-1" points="81.26 35.47 88.94 37.45 92.88 38.47 90.47 26.92 81.26 35.47"></polygon>
            <polygon className="cls-1" points="81.42 36.12 87.71 44.18 92.71 39.02 86.11 37.33 81.42 36.12"></polygon>
            <polygon className="cls-1" points="126.46 41.01 126.67 41.54 130.32 41.43 126.24 40.45 126.46 41.01"></polygon>
            <polygon className="cls-1" points="122.56 63.02 123.5 58.21 120.63 58.44 122.56 63.02"></polygon>
            <polygon className="cls-1" points="117.78 60.9 116.86 61.78 122.14 63.52 120.1 58.69 117.78 60.9"></polygon>
            <polygon className="cls-1" points="5.35 49.39 4.57 50.96 4.95 51.13 6.53 51.81 6.21 47.67 5.35 49.39"></polygon>
            <polygon className="cls-1" points="116.66 62.33 117.33 67.04 117.48 68.11 120.08 65.86 122.08 64.12 117.73 62.68 116.66 62.33"></polygon>
            <polygon className="cls-1" points="123.33 57.64 123.58 57.62 123.63 51.24 120.67 57.86 123.33 57.64"></polygon>
            <polygon className="cls-1" points="81.56 93.86 76.56 90.09 73.89 96.68 79.39 94.66 81.56 93.86"></polygon>
            <polygon className="cls-1" points="117.15 72.94 114.03 70.38 112.57 72.58 114.18 72.7 117.15 72.94"></polygon>
            <polygon className="cls-1" points="122.82 39.38 123.19 40.51 123.27 40.76 123.29 40.75 124.92 40.07 122.82 39.38"></polygon>
            <polygon className="cls-1" points="114.54 70.04 114.78 70.23 117.63 72.58 117.04 69.08 114.54 70.04"></polygon>
            <polygon className="cls-1" points="124.07 41.05 126.01 41.44 125.6 40.42 124.07 41.05"></polygon>
            <polygon className="cls-1" points="103.53 72.29 102.06 71.02 101.49 70.53 101.53 72.45 102.12 72.4 103.53 72.29"></polygon>
            <polygon className="cls-1" points="105.53 74.17 104.41 73.09 104.2 74.01 103.87 75.42 104.05 75.28 105.53 74.17"></polygon>
            <polygon className="cls-1" points="101.76 73.02 103.28 75.39 103.87 72.85 101.76 73.02"></polygon>
            <polygon className="cls-1" points="103.87 71.82 103.74 70.67 102.2 70.37 102.7 70.81 103.87 71.82"></polygon>
            <polygon className="cls-1" points="108.65 74.44 106.44 74.49 107.55 76.87 108.41 74.98 108.65 74.44"></polygon>
            <polygon className="cls-1" points="105.87 74.65 105.25 75.12 104.02 76.03 106.99 77.07 106.96 76.99 105.87 74.65"></polygon>
            <polygon className="cls-1" points="109.24 74.57 108.04 77.19 112.31 76.45 111.31 75.84 109.24 74.57"></polygon>
            <polygon className="cls-1" points="109.74 74.18 112.64 75.97 111.86 73.23 109.74 74.18"></polygon>
            <polygon className="cls-1" points="113.3 76.14 117.19 73.53 112.44 73.15 113.3 76.14"></polygon>
            <polygon className="cls-1" points="117.58 68.8 118.18 72.31 118.19 72.37 121.75 65.19 118.23 68.23 117.58 68.8"></polygon>
            <polygon className="cls-1" points="23.75 87.72 15.93 91.05 20.78 92.86 23.75 87.72"></polygon>
            <polygon className="cls-1" points="23.91 88.62 21.41 92.95 21.94 92.96 26.17 93.1 24.38 87.81 23.91 88.62"></polygon>
            <polygon className="cls-1" points="17.58 70.74 22.7 67.82 11.5 59.8 15.73 71.8 17.58 70.74"></polygon>
            <polygon className="cls-1" points="25.48 54.61 22.6 52.94 15.64 48.91 13.98 52.73 11.43 58.58 16.8 57.06 25.48 54.61"></polygon>
            <polygon className="cls-1" points="75.86 90.28 74.39 91.1 68.18 94.55 73.22 96.8 74.17 94.45 75.86 90.28"></polygon>
            <polygon className="cls-1" points="6.87 47 10.94 58.24 15.1 48.71 6.87 47"></polygon>
            <polygon className="cls-1" points="23.79 97.63 28.27 97.12 26.54 93.94 25.28 95.64 23.79 97.63"></polygon>
            <polygon className="cls-1" points="23.72 93.61 21.37 93.53 21.64 94.09 23.21 97.43 26.01 93.68 23.72 93.61"></polygon>
            <polygon className="cls-1" points="16.84 97.54 22.69 97.69 20.81 93.72 16.84 97.54"></polygon>
            <polygon className="cls-1" points="18.29 92.56 15.51 91.51 16.34 97.21 20.37 93.34 18.29 92.56"></polygon>
            <polygon className="cls-1" points="15.61 84.62 15.45 90.62 23.61 87.14 15.61 84.62"></polygon>
            <polygon className="cls-1" points="6 44.29 5.57 36.59 4.23 46.31 6.11 46.31 6 44.29"></polygon>
            <polygon className="cls-1" points="5.94 46.9 4.21 46.9 4.39 50 5.09 48.62 5.94 46.9"></polygon>
            <polygon className="cls-1" points="6.27 66.53 6.63 65.38 6 64.53 6.21 66.74 6.27 66.53"></polygon>
            <polygon className="cls-1" points="4.51 51.57 4.83 54.41 6.41 52.39 4.51 51.57"></polygon>
            <polygon className="cls-1" points="6.89 76.01 7.07 71.29 5.75 73.62 6.89 76.01"></polygon>
            <polygon className="cls-1" points="6.25 61.52 5.55 60.83 5.77 62.51 6.25 61.52"></polygon>
            <polygon className="cls-1" points="5.88 72.2 6.21 71.62 7.06 70.11 6.26 68.99 5.88 72.2"></polygon>
            <polygon className="cls-1" points="4.99 55.92 5.33 58.98 6.24 57.11 4.99 55.92"></polygon>
            <polygon className="cls-1" points="6.46 60.9 6.35 58.22 5.55 59.86 5.51 59.95 5.8 60.25 6.46 60.9"></polygon>
            <polygon className="cls-1" points="6.38 68.16 7.01 69.05 6.8 66.79 6.38 68.16"></polygon>
            <polygon className="cls-1" points="64.11 55.75 57.57 59.82 57.43 59.9 64.85 68.7 64.23 57.85 64.11 55.75"></polygon>
            <polygon className="cls-1" points="66.87 56.64 64.7 55.7 65.44 68.93 75.09 60.19 66.87 56.64"></polygon>
            <polygon className="cls-1" points="47.41 32.72 56.73 48.35 58.91 31.63 47.41 32.72"></polygon>
            <polygon className="cls-1" points="70.81 77.14 77.11 72.07 65.79 69.96 70.48 77.4 70.81 77.14"></polygon>
            <polygon className="cls-1" points="57.2 49.83 57.21 50.58 57.28 59.31 63.87 55.22 57.2 49.83"></polygon>
            <polygon className="cls-1" points="70.83 84.35 70.31 83.83 70.08 89.42 75.74 89.34 70.83 84.35"></polygon>
            <polygon className="cls-1" points="70.57 90 69.96 90 68.17 93.88 75.27 89.93 70.57 90"></polygon>
            <polygon className="cls-1" points="67.53 89.51 65.08 88.97 67.52 93.89 69.36 89.92 67.53 89.51"></polygon>
            <polygon className="cls-1" points="117.71 39.01 118.61 38.58 119.49 38.15 116.79 36.93 117.39 38.29 117.71 39.01"></polygon>
            <polygon className="cls-1" points="69.5 89.35 69.57 87.73 69.72 83.87 65.14 88.38 65.94 88.56 69.5 89.35"></polygon>
            <polygon className="cls-1" points="32.53 64.61 23.59 68.02 24.16 70.17 25.43 74.98 29.44 70.44 29.44 70.44 35.65 63.41 32.53 64.61"></polygon>
            <polygon className="cls-1" points="45.92 97.34 45.34 94.65 36.3 96.47 45.92 97.34"></polygon>
            <polygon className="cls-1" points="35.87 76.62 30.46 71.74 34.53 86.11 39.41 79.82 35.87 76.62"></polygon>
            <polygon className="cls-1" points="37.74 88.14 34.69 87.14 34.59 95.96 42.21 89.62 37.74 88.14"></polygon>
            <polygon className="cls-1" points="17.13 84.49 24.18 86.72 24.94 76.27 15.91 84.1 17.13 84.49"></polygon>
            <polygon className="cls-1" points="42.09 46.38 45.09 53.48 48.76 62.16 56.45 49.41 42.09 46.38"></polygon>
            <polygon className="cls-1" points="35.36 96.07 37.4 95.66 45.11 94.09 42.73 89.95 41.01 91.38 35.36 96.07"></polygon>
            <polygon className="cls-1" points="33.31 66.94 30.07 70.61 30.09 70.63 38.98 78.65 39.66 79.26 44.65 63.07 36.78 63.01 33.31 66.94"></polygon>
            <polygon className="cls-1" points="39.68 53.5 37.04 62.43 48.28 62.51 41.66 46.84 39.68 53.5"></polygon>
            <polygon className="cls-1" points="34.89 86.6 38.13 87.66 42.36 89.05 39.69 80.41 34.89 86.6"></polygon>
            <path className="cls-1" d="M171.23,59.37h-7.11q-7,0-9.93,3.09t-2.88,9.09V86.74q0,6,2.88,9.09t9.93,3.08h16.72V93.38H164.12c-3.17,0-5.26-.54-6.29-1.62s-1.54-2.73-1.54-5V81.91L184,81.67l0-10.08q0-6-2.89-9.09T171.23,59.37Zm7.83,17H156.29V71.55q0-3.43,1.54-5c1-1.07,3.12-1.6,6.29-1.6h7.11c3.17,0,5.26.53,6.29,1.6s1.54,2.75,1.54,5Zm35.51-17h5.06L206.9,98.91h-6.45L187.76,59.37h5.06L203.7,93.26Zm30.05,0H237.5q-7,0-9.92,3.09c-1.92,2.08-2.89,5.11-2.89,9.09V86.74c0,4,1,7,2.89,9.09s5.26,3.08,9.92,3.08h16.73V93.38H237.5q-4.74,0-6.28-1.62t-1.54-5V81.91l27.71-.24,0-10.08q0-6-2.88-9.09T244.62,59.37Zm7.83,17H229.68V71.55q0-3.43,1.54-5t6.28-1.6h7.12q4.74,0,6.29,1.6t1.54,5ZM318.09,86.7v1.5q0,5.3-2.91,8t-8.48,2.7H291.08V93.38H306.7q3.36,0,4.89-1.29a4.81,4.81,0,0,0,1.52-3.89V86.7a4.84,4.84,0,0,0-1.52-3.9q-1.53-1.27-4.89-1.28h-5.3q-7,0-9.9-2.49c-1.94-1.67-2.91-4.14-2.91-7.44V70.05c0-3.51,1-6.16,2.91-8s5.24-2.71,9.9-2.71h11.71v5.54H301.4q-4.78,0-6.3,1.26a4.8,4.8,0,0,0-1.52,3.92v1.46a3.84,3.84,0,0,0,1.5,3.32Q296.58,76,301.4,76h5.3q5.58,0,8.48,2.71T318.09,86.7ZM358.5,59.37h5V98.91h-5Zm13.05-15.81h5V98.91h-5Zm17.08,0V98.91h-5V43.56Zm-30.13,0h5v5h-5Zm-4,15.81L334.73,79.14l19.81,19.77h-6.68l-17.95-17h-.16v17h-5V43.56h5V76.38h.16l17.95-17Zm-76.39,0h5.7v5.54h-5.7q-4.74,0-6.28,1.6t-1.54,5V98.91h-5V71.55c0-4,1-7,2.89-9.09S273.49,59.37,278.15,59.37Z"></path>
        </svg>
    </a>
}

export default Logo