import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

import EnvInfo from './EnvInfo'

it('renders without crashing', () => {
    const wrapper = Enzyme.mount(<EnvInfo />)
    expect(wrapper!=undefined).toBe(true)
});
