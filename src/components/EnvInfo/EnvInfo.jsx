import React from 'react';
import styles from './EnvInfo.scss';
import {environments} from 'utils/constants';

const EnvInfo =()=> {
        switch(process.env.NODE_ENV) {
            case environments.production:
                return null;
            default:
                return (
                    <div className={styles.foo}>
                        {process.env.NODE_ENV}
                    </div>
                );
        }
}

export default EnvInfo