import React from 'react';
import PropTypes from 'prop-types';

export default class CountdownComponent extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
             secondsRemaining: props.secondsRemaining
        }
    }
    tick() {
        this.setState({secondsRemaining: this.state.secondsRemaining - 1});
        if (this.state.secondsRemaining <= 0) {
            clearInterval(this.interval);
            this.props.onComplete();
        }
    }
    componentDidMount() {
        const tick = this.tick.bind(this);
        this.interval = setInterval(()=>{tick()}, 1000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    render() {
        return (
            <div>{this.state.secondsRemaining}</div>
        );
    }
}

CountdownComponent.propTypes = {
    secondsRemaining:PropTypes.number,
    onComplete: PropTypes.func,
}
CountdownComponent.defaultProps = {
    secondsRemaining:0,
}