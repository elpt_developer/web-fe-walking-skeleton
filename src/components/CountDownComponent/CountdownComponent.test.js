import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

import CountdownComponent from './CountdownComponent'

it('renders without crashing', () => {
    const wrapper = Enzyme.mount(<CountdownComponent />)
    expect(wrapper!=undefined).toBe(true)
});