import React from 'react';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import styles from './Menu.scss';
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import {Row, Button} from 'react-bootstrap'

export const Menu = ({routing, changePage}) => (
    <Row className={styles.menu}>
        {/* Alternative way to redirect to another route : with <Link> or with action */}
        <Button onClick={() => changePage('/')}>Home</Button>&nbsp;
        <Button onClick={() => changePage('/about-us')}>About</Button>
        &nbsp;&nbsp;&nbsp;testing libraries: &nbsp;&nbsp;
        <Link to="/Font-Awesome">Font-Awesome</Link>&nbsp;&nbsp;
        <Link to="/recompose">Recompose.JS</Link>&nbsp;&nbsp;
        <Link to="/redux">Redux</Link>&nbsp;&nbsp;
        <Link to="/bootstrap">Bootstrap</Link>&nbsp;&nbsp;
        <Link to="/images">Images</Link>&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;Check out the error page: &nbsp;&nbsp;
        <Link to="/error">this is not funny</Link>&nbsp;&nbsp;
    </Row>
)
Menu.propTypes = {
    routing: PropTypes.object
}
Menu.defaultProps =  {
    routing: {}
}

const mapStateToProps = state => ({
    routing: state.routing,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    changePage: (page) => push(page)
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Menu)