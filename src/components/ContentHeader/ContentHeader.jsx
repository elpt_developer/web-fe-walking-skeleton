import React from 'react'
import PropTypes from 'prop-types';
import styles from './ContentHeader.scss'
import {Row, Col} from 'react-bootstrap'

const ContentHeader = (props) => {
    const styleWeb = props.bgWeb ? {
        backgroundImage: `url(${props.bgWeb})`,
        backgroundSize: 'cover'
    } : {}
    const styleMobile = props.bgMobile ? {
        backgroundImage:`url(${props.bgMobile})`,
        backgroundSize: 'contain'
    } : {}
    return <div className={styles['container']}>
        <Row>
            {/* WEB UI */}
            <Col xsHidden sm={12} md={12} lg={12} className={styles['web']} style={styleWeb} >
                <Row className={styles['quote']}>
                    <Col md={8} mdOffset={4}>
                        <h2>{props.quote}</h2>
                        <p>{props.author}</p>
                    </Col>
                </Row>
                <h1 className={styles['title']}>{props.title}</h1>
            </Col>

            {/* MOBILE UI */}
            <Col xs={12} smHidden mdHidden lgHidden className={styles['mobile']}>
                <h1  className={styles['title']}>{props.title}</h1>
                <Row className={styles['quote-mobile']} style={styleMobile}>
                    <Col md={12}>
                        <h2>{props.quote}</h2>
                        <p>{props.author}</p>
                    </Col>
                </Row>
            </Col>
        </Row>
    </div>
}

ContentHeader.propTypes = {
    title: PropTypes.string,
    quote: PropTypes.string,
    author: PropTypes.string,
    bgWeb: PropTypes.string,
    bgMobile: PropTypes.string
}
ContentHeader.defaultProps = {
    title:``,
    quote:``,
    author:``,
    bgWeb: undefined,
    bgMobile: undefined
}

export default ContentHeader