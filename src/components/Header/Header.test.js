import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

import Header from './Header'

it('renders without crashing', () => {
    const props = {}
    const wrapper = Enzyme.mount(<Header {...props}/>)
    expect(wrapper!=undefined).toBe(true)
});