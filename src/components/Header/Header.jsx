import React from 'react';
import styles from './Header.scss';
import Logo from 'components/Logo/Logo'
import EnvInfo from 'components/EnvInfo/EnvInfo'

const Header = () => <div className={styles['Header']}>
    <div className={styles['Header-logo']}><Logo /></div>
    <div className={styles['Header-content']}>
        <div className={styles["Header-content-envInfo"]}><EnvInfo/></div>
    </div>
</div>

export default Header
