
import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

import Button from './Button'

it('renders without crashing', () => {
    const props = {}
    const wrapper = Enzyme.mount(<Button {...props}/>)
    expect(wrapper!=undefined).toBe(true)
});