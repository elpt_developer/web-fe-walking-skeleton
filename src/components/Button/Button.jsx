import React from 'react'
import PropTypes from 'prop-types'
import styles from './Button.scss'

const Button = ({callback, children}) => <button className={styles['button']}>{children}</button>
Button.proptypes = {
    children: PropTypes.node,
    callback: PropTypes.func
}
export default Button