import {endpoints} from "api/endpoints.config";
import {fetchAsync} from 'api/fetchAsync'

export const api = {}

api.getUserByActivationKey = async (activationKey) => {
    const params = {
        activationKey:activationKey
    };
    const {url, requestHeaders} = endpoints.userByActivationKey(params);
    return await fetchAsync(url, requestHeaders)
}