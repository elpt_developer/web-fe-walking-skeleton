'use strict';
import {api} from './api'
jest.dontMock('./api')

describe('[api.async] methods', () => {
    const generateFakeResponse = (json, status)=> new window.Response(json?json:'{"uuid":"uuid"}', {
        status: status?status:200,
        headers: {
            'Content-type': 'application/json'
        }
    })
    beforeEach(function() {
        window.fetch = jest.fn().mockImplementation(() => Promise.resolve(generateFakeResponse()));
    })
    it('test api.getTrainingV1', async ()=> await api.getUserByActivationKey(1))
})