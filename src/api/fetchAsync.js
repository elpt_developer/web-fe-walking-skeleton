require('es6-promise').polyfill();
require('isomorphic-fetch');
require('babel-polyfill')

export const fetchAsync = async (url, headers, noBodyResponse) => {
    let response = await fetch(url, headers);
    switch(response.status) {
        case 401:
        case 400:
        case 404:
        case 409:
        case 500:
            let error = await response.json()
            throw error
        default:
            return noBodyResponse?{}:await response.json()
    }
}