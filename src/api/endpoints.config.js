import {methods} from "utils/constants";

const envConfigs = ()=> {
    switch(process.env.NODE_ENV) {
        case '________production': /* TODO remove ______ once developments ends*/
            return {
                BASE_URL: 'https://api.ops.everskill.de/1.0',
                CLIENT_ID: 'everskill-app-trainerinterface',
                CLIENT_SECRET: 'b63b12de336eeafd1dedb440b304ab41',
                MIX_PANEL_TOKEN: '087fa59ae59eb8beef8d11eb8d49ed14',
            }
        default:
            return {
                BASE_URL: "https://api1.ops.everskill.de",
                CLIENT_ID: 'everskill-app-trainerinterface',
                CLIENT_SECRET: "b63b12de336eeafd1dedb440b304ab41",
                MIX_PANEL_TOKEN: "06a11891c9fe42286a1758b2d73b821f",
            }
    }
}

var endpoints = {
    userByActivationKey : (params) => {
        return {
            url : `${envConfigs().BASE_URL}/users/by-activationkey/${params.activationKey}`,
            requestHeaders : {
                method: methods.GET,
                headers: {
                    //Authorization: `Bearer ${params.access_token}`,
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
            }
        }
    }
};
module.exports = {endpoints};