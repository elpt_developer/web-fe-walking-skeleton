import cookie from "react-cookies";

const EN = 'EN', DE = 'DE';
export const calculateLanguageByNavigator = ()=> {
    switch(navigator.language.split('-')[1]) {
        case DE:
            return DE
        default:
            return EN
    }
}
export const setLanguage = (lang) => cookie.save('lang', lang, { path: '/' })
export const getLanguage = ()=> {
    const cookies = cookie.loadAll();
    switch(cookies['lang']) {
        case DE:
            return DE
        case undefined:
            const lang = calculateLanguageByNavigator()
            setLanguage(lang)
            return lang
        default:
            return EN
    }
}
export const changeLanguage = (lang) => {
    setLanguage(lang)
    window.location.reload()
}