// ------------------------------------
// Constants
// ------------------------------------

export const environments = {
    OFFLINE: "OFFLINE",
    development:"development",
    staging:"staging",
    production:"production"
};
export const methods = {
    GET: 'get',
    POST: 'post',
    PUT: 'put',
    PATCH: 'patch',
    DELETE: 'delete'
}