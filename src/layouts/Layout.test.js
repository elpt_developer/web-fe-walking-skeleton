import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });

import Layout from './Layout'

it('renders without crashing', () => {
    const wrapper = Enzyme.mount(<Layout />)
    expect(wrapper!=undefined).toBe(true)
});
