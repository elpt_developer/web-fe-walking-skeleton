import React from 'react'
import styles from './Layout.scss'
import Header from 'components/Header/Header'
import intl from 'react-intl-universal'
import {getLanguage} from 'utils/i18l'

const locales = {
	"EN": require('locales/en-US.json'),
	"DE": require('locales/de-DE.json'),
}

class Layout extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            i18nInit:false,
            scrolled: false,
            headerHeight:0
        }
    }
    componentDidMount() {
        /* This code will define the margin top of the app main content depends on the screen height of the header.
            this will be necessary for handling different platform header's height. */
        const headerHeight = this.headerElement.clientHeight;
        this.setState({ headerHeight });
        /* This will handle a different rendering of the header depending on the scroll*/
        window.addEventListener('scroll', this.handleScroll.bind(this));
	
        // load locales
        intl.init({
            currentLocale: getLanguage(), // determine locale
            locales,
        }).then(()=>this.setState({i18nInit:true}))
    }
    componentWillUnmount() {
        /* Delete the scroll event listener */
        window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll(event) {
        const domElememt = event.srcElement || event.target
        this.setState({scrolled: domElememt.scrollingElement.scrollTop>0})
    }
    render() {
        const props = this.props
        const headerClassname = this.state.scrolled?"layout-header-scrolled":"layout-header"
        return <div className={styles['layout']}>
            <header className={styles[headerClassname]}
                    ref={ (headerElement) => this.headerElement = headerElement}>
                <Header/>
            </header>
            {this.state.i18nInit &&
                <main className={styles["layout-main"]} style={{marginTop:this.state.headerHeight}}>
                    {props.children}
                </main>
            }
        </div>
    }
}

export default Layout
