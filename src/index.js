import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from 'store/store'
import Layout from 'layouts/Layout'
import DevTools from 'containers/DevTools'
import 'babel-polyfill';

import 'statics/css/index.css'
import Routes from 'containers/Routes/Routes'

const target = document.querySelector('#root')
console.log("Process Evn mode="+JSON.stringify(process.env))
render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Layout>
                <Routes/>
                {process.env["NODE_ENV"]==="development" && <DevTools/>}
            </Layout>
        </ConnectedRouter>
    </Provider>,
    target
)