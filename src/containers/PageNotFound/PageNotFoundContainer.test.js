
import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
import {PageNotFoundContainer} from './PageNotFoundContainer'
import * as constants from 'utils/constants'

function setup(jestCompatibility) {
    const props = {
        jestCompatibility:jestCompatibility
    }
    const enzymeWrapper = Enzyme.shallow(<PageNotFoundContainer {...props} />);
    return {
        props,
        enzymeWrapper
    }
}

describe('components', () => {
    describe('PageNotFoundContainer', () => {
        it('should render self and subcomponents', () => {
            const { enzymeWrapper } = setup(true)
            expect(true).toBe(true)
        })
        it('should render self and subcomponents', () => {
            const { enzymeWrapper } = setup()
            expect(true).toBe(true)
        })
    })
})
