import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './PageNotFoundContainer.scss';
import {Row, Col} from 'react-bootstrap';
import CountDownComponent from 'components/CountDownComponent/CountDownComponent'


export class PageNotFoundContainer extends React.Component {
    redirect() {
        window.location.replace("/");
    }
    render() {
        return (
            <Row >
                <Col md={12} sm={12}  className={styles.PageNotFoundContent}>
                    <h1>Ops!</h1>
                    <h2>Page not found.</h2>
                    <br />
                    <br />
                    <Row>
                        <Col md={12} xs={12}>
                            <p>Redirecting in seconds...</p>
                        </Col>
                        <Col md={12} xs={12}>
                            <div>
                                <CountDownComponent secondsRemaining={5} onComplete={this.redirect.bind(this)}/>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>

        );
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

export default connect((mapStateToProps), {
})(PageNotFoundContainer);

