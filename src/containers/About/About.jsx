import React from 'react'
import intl from 'react-intl-universal'

export default () => {
    const hello = intl.get('HELLO');
    return <div>
        <h1>{hello}</h1>
        <p>This is a walking skeleton configured with a preset of libraries defined for Everskill Web Apps.</p>
    </div>
}