import React from 'react';
import { createDevTools } from 'redux-devtools';
// import LogMonitor from 'redux-devtools-log-monitor';
// import SliderMonitor from 'redux-slider-monitor';
import DockMonitor from 'redux-devtools-dock-monitor';
import Inspector from 'redux-devtools-inspector';


/* Working alternatives */
// const classicDockMonitor = createDevTools(
//     <DockMonitor toggleVisibilityKey='ctrl-h'
//                  defaultPosition='bottom'
//                  changePositionKey='ctrl-q'
//                  changeMonitorKey='ctrl-m'
//                  defaultIsVisible={false}>
//         <LogMonitor />
//         <SliderMonitor />
//     </DockMonitor>
// );

export default createDevTools(
    <DockMonitor toggleVisibilityKey='ctrl-h'
                 changePositionKey='ctrl-q'
                 changeMonitorKey='ctrl-m'
                 defaultPosition='bottom'
                 defaultIsVisible>
        <Inspector />
    </DockMonitor>
);