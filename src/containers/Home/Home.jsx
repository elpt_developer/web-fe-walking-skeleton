import React from 'react'
import {Row, Button} from 'react-bootstrap'
import {push} from "react-router-redux";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

const Home = ({changePage}) => <div>
    <h1>Home</h1>
    <Row>Welcome home!</Row>
    <Row><Button onClick={() => changePage('/')}>Home</Button></Row>
    <Row><Button onClick={() => changePage('/about-us')}>About</Button></Row>
    <hr/>
    <h2>AWS</h2>
    <Row><Button onClick={() => changePage('/aws-sdk')}>Test SDK</Button></Row>
    <hr/>
    <h2>Test plugins</h2>
    <Row><Button onClick={() => changePage('/font-awesome')}>Font-Awesome</Button></Row>
    <Row><Button onClick={() => changePage('/redux')}>Redux Store</Button></Row>
    <Row><Button onClick={() => changePage('/bootstrap')}>Bootstrap Grid System</Button></Row>
    <Row><Button onClick={() => changePage('/multilanguage')}>Multilanguage</Button></Row>
    <hr/>
    <h2>Check out the error page</h2>
    <Row><Button onClick={() => changePage('/error')}>this is not funny</Button></Row>
</div>

const mapStateToProps = state => ({
    routing: state.routing,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    changePage: (page) => push(page)
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)