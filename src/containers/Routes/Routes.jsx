import React from 'react';
import { Route, Switch } from 'react-router-dom'
import Home from 'containers/Home/Home'
import About from 'containers/About/About'
import FontAwesomeTest from 'containers/Tests/FontAwesomeTest'
import ReduxTest from 'containers/Tests/ReduxTest'
import BootstrapTest from 'containers/Tests/BootstrapTest'
import ImagesTest from 'containers/Tests/ImagesTest'
import { TransitionGroup, CSSTransition } from "react-transition-group";
import {connect} from "react-redux";
import ActivationProcess from 'containers/ActivationProcess/ActivationProcess'
import ErrorPage from 'containers/ErrorPage/ErrorPage'
import InvalidLink from 'containers/InvalidLink/InvalidLink'
import Multilanguage from 'containers/Tests/Multilanguage'
import AWS_SDK_Test from 'containers/Tests/AWS_SDK_Test'

const mapStateToProps = state => ({
    routing: state.routing,
})



const Routes = (props) =><TransitionGroup>
    <CSSTransition key={props.routing.location.key} classNames="SlideIn" timeout={500}>
        <Switch location = {props.routing.location}>
            <Route exact path="/" component={Home} />
            <Route exact path="/activation/:activationKey" component={ActivationProcess} />
            <Route exact path="/about-us" component={About} />
            <Route exact path="/font-awesome" component={FontAwesomeTest} />
            <Route exact path="/redux" component={ReduxTest} />
            <Route exact path="/bootstrap" component={BootstrapTest} />
            <Route exact path="/images" component={ImagesTest} />
            <Route exact path="/error" component={ErrorPage} />
            <Route exact path="/multilanguage" component={Multilanguage} />
            <Route exact path="/aws-sdk" component={AWS_SDK_Test} />
            <Route component={InvalidLink} />
        </Switch>
    </CSSTransition>
</TransitionGroup>

export default connect(
    mapStateToProps
)(Routes)