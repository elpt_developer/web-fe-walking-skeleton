import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
import {Provider} from 'react-redux'
import mockStore from 'redux-mock-store'

import Routes from './Routes'
import { ConnectedRouter } from 'react-router-redux'
import {history} from "store/store";

it('renders without crashing', () => {
    const store = mockStore()({
        routing:{"location":{"pathname":"/about-us","search":"","hash":"","key":"ujnvrd"}}
    })
    const wrapper = Enzyme.mount(<Provider store={store}>
        <ConnectedRouter history={history}>
            <Routes />
        </ConnectedRouter>
    </Provider>)
    expect(wrapper!=undefined).toBe(true)
});
