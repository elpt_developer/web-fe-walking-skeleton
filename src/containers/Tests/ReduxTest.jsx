import React from 'react';
import { connect } from 'react-redux'
import {Row} from 'react-bootstrap'

const ReduxTest = (props) => (
    <Row>
        <h3>Connected to Redux Store:</h3>
        <p>{JSON.stringify(props.routing)}</p>
    </Row>
)
ReduxTest.propTypes = {
}
ReduxTest.defaultProps =  {
}

const mapStateToProps = state => ({
    routing: state.routing,
})

export default connect(
    mapStateToProps
)(ReduxTest)