import React from 'react'
import intl from 'react-intl-universal'
import {changeLanguage} from 'utils/i18l'

export default () => [
    <h1 key={'hello'}>{intl.get('HELLO')}</h1>,
    <p key={'changeLanguage-btns'}>
        <button onClick={()=>changeLanguage('EN')}>en-US</button>
        <button onClick={()=>changeLanguage('DE')}>de-DE</button>
    </p>
]