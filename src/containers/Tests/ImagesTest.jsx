import React from 'react'
import {Row, Col} from 'react-bootstrap'
import jpg from 'statics/images/jpg.jpg';
import png from 'statics/images/IndianStub.png';
import svg from 'statics/images/logo.svg';

export default () => <div>
    <h1>Import and render Images</h1>
    <Row>
        <Col md={6}><h2>.jpg example</h2></Col>
        <Col md={6}><img src={jpg} style={{maxHeight:"150px"}} alt={"test"} /></Col>
    </Row>
    <Row>
        <Col md={6}><h2>.png example</h2></Col>
        <Col md={6}><img src={png} style={{maxHeight:"150px"}} alt={"test"} /></Col>
    </Row>
    <Row>
        <Col md={6}><h2>.svg example</h2></Col>
        <Col md={6}><img src={svg} style={{maxHeight:"150px"}} alt={"test"}/></Col>
    </Row>
</div>