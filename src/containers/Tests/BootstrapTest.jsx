import React from 'react'
import {Row, Col, Button} from 'react-bootstrap'

const BootstrapTest = props => <Row>
    <h1>Testing React-Bootstrap Grid System</h1>
    <Row>
        <Col md={6}>
            Testing Bootstrap gridsystem
        </Col>
        <Col md={6}>
            <Button onClick={() => props.changePage()}>Go to about page via redux</Button>
        </Col>
    </Row>
</Row>

export default BootstrapTest