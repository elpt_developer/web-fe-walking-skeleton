import React from 'react'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

import fontawesome from '@fortawesome/fontawesome'

import faUser from '@fortawesome/fontawesome-free-solid/faUser'
import faCircle from '@fortawesome/fontawesome-free-regular/faCircle'
import faFacebook from '@fortawesome/fontawesome-free-brands/faFacebook'

fontawesome.library.add(faUser, faCircle, faFacebook)

export default () => (
    <div>
        <h1>FontAwesomeTest</h1>
        <FontAwesomeIcon icon={faUser} size="6x"/>&nbsp;
        <FontAwesomeIcon icon={faCircle} size="6x" style={{color:"lightgray"}}/>&nbsp;
        <FontAwesomeIcon icon={faFacebook} size="6x" style={{color:"blue"}}/>
    </div>
)