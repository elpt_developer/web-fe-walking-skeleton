import React from 'react'
import PropTypes from 'prop-types';
import {Row, Glyphicon} from 'react-bootstrap'
import Dropzone from 'react-dropzone'
import AWS from 'aws-sdk';
// Initialize the Amazon Cognito credentials provider
AWS.config.region = 'eu-central-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'eu-central-1:4ac08800-2406-4fb1-bfd0-247c296350fc',
});
const bucket = 'spike.wf-2157'
var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {Bucket: bucket}
});
s3.getBucketAcl({Bucket: bucket}, function(err, data) {
    if (err) {
        console.log("Error", err);
    } else if (data) {
        console.log("Success", data.Grants);
    }})
const delimiter = '/'
const loadFrom = (prefix) => new Promise(function(resolve, reject) {
    s3.listObjects({Delimiter:delimiter, Prefix: prefix}, function(err, data) {
        if (err)    reject(err)
        else        {
            data['href'] = this.request.httpRequest.endpoint.href
            resolve(data)
        }
    })
});
const createObject = (key, file) => new Promise(function(resolve, reject) {
    s3.putObject({
        Key: key,
        ContentType: file.type,
        Body: file,
        ACL: 'public-read'
    }, function(err, data) {
        if (err) {
            reject(err)
        }
        resolve(data)
    });
});

const style = {
    cursor:"pointer",
    textAlign: "left"
}
const childStyle = {
    paddingLeft:"20px"
}


class Folder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            contents: undefined
        }
    }
    async toggleOpen() {
        if (this.state.open) {
            this.setState({
                open:false,
                contents: undefined
            })
        } else {
            this.setState({
                open: true,
                contents: await(loadFrom(this.props.folder))
            })
        }
    }
    async onDrop(acceptedFiles, rejectedFiles, folder) {
        if (acceptedFiles.length===0)    return
        const file = acceptedFiles[0]
        const key = (this.props.folder||'') + file['name']
        const created = await createObject(key, file)
        console.log(JSON.stringify(created))
        this.setState({
            open: true,
            contents: await loadFrom(this.props.folder)
        })
    }
    render () {
        let pathLabel = ''
        const paths = this.props.folder.split('/').filter(e=>e)
        if (paths.length>0) pathLabel = paths[paths.length-1]
        const overlayStyle = {
            top: 0,
            right: 0,
            background: 'rgba(0,0,0,0.1)',
            textAlign: 'center',
            color: '#fff'
        };
        return [
            <Row  key={'Folder'} style={style} onClick={()=>this.toggleOpen()}>
                <Glyphicon glyph={this.state.open ? "folder-open" : "folder-close"}/>&nbsp;&nbsp;{pathLabel}
            </Row>,
            this.state.contents && <div key={'FolderContent'}
                // key={'folders'}
                style={childStyle}>
                {this.state.contents['CommonPrefixes'].map(e=><RFolder key={e['Prefix']} folder={e['Prefix']}/>)}
                {this.state.contents['Contents']
                    .filter(e=>e['Key']!==this.props.folder) // the folder itself is a content => we will filter the element because rendered by the folder parent
                    .map(e=>{
                        const href = `${this.state.contents['href']}${bucket}`
                        const fullPath = e['Key']
                        const filename = fullPath.replace(this.props.folder, '')
                        return <Link key={fullPath} filename={filename} href={`${href}/${fullPath}`}/>
                    })
                }
            </div>,
            this.state.open && <Dropzone key={'Upload'} style={{position: "relative"}} onDrop={this.onDrop.bind(this)}>
                <div style={overlayStyle}>Drop files...</div>
            </Dropzone>
        ]
    }
}
Folder.propTypes = {
    folder: PropTypes.string,
    create: PropTypes.bool
}
Folder.defaultProps = {
    folder: '',
    create: true
}
const RFolder = (props) => <Folder {...props}/>
const Link = ({filename, href}) => <Row style={style}><Glyphicon glyph={"file"}/>&nbsp;&nbsp;<a href={href} target="_blank">{filename}</a></Row>

// const CreateFileStyle = Object.assign({}, style, childStyle, {fontSize:"small", color:"gray"})
// const CreateFile = ({path, onClick}) => <Row style={CreateFileStyle} onClick={onClick}>Create new file</Row>


export default ()=> <div style={{maxWidth:"500px", margin:"auto"}}>
    <h1>Testing AWS SKD</h1>
    <Folder create={false}/>
</div>