import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
import ActivationProcess from './ActivationProcess'

it('renders without crashing', () => {
    const props = {
        match: {
            params: {
                activationKey:'example'
            }
        },
        getUserByActivationKey: new Promise( (res, rej)=> {
            res({firstName:""})
        })
    }
    const wrapper = Enzyme.shallow(<ActivationProcess {...props}/>)
    expect(wrapper!=undefined).toBe(true)
});