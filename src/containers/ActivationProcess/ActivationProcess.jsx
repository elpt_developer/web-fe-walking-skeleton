import React from 'react';
import styles from './ActivationProcess.scss'
import {api} from 'api/api'
import ContentHeader from 'components/ContentHeader/ContentHeader'
import {Row, Col} from 'react-bootstrap'
import Button from 'components/Button/Button'
import InvalidLink from 'containers/InvalidLink/InvalidLink'

class ActivationProcess extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: undefined,
            error: undefined
        }
    }
    componentWillMount() {
        const activationKey = this.props.match.params.activationKey
        const setState = this.setState.bind(this)
        api.getUserByActivationKey(activationKey)
            .then(
                data=>setState({firstName:data.firstName}),
                err =>setState({error:err.message})
            )
    }
    render() {
        if (this.state.error) return InvalidLink()
        switch (this.state.firstName) {
            case undefined:
                return <p>loading...</p>
            default:
                return[
                    <ContentHeader key={'ContentHeader'}
                        title={`Welcome ${this.state.firstName}, get started with everskill`}
                        quote={'"Everskill helped me to reach my goals - they enabled me to become a better leader."'}
                        author={'Daniel Krauß, Founder & CIO at Flixbus'}
                    />,
                    <div key={'Content'} className={styles.container}>
                        <Row>
                            <Col md={7}>
                                <h2>{'Your digital coach will support you to reach your goals.'}</h2>
                            </Col>
                            <Col md={2} style={{paddingTop:"20px"}}>
                                <Button>{'Get started'}</Button>
                            </Col>
                        </Row>
                    </div>
                ]
        }
    }
}
ActivationProcess.propTypes = {
}
ActivationProcess.defaultProps = {
}
export default ActivationProcess