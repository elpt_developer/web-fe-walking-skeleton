import React from 'react'
import styles from './ErrorPage.scss'

export default (props)=> <div className={styles['error-container']}>
    <h2>ERROR</h2>
    <br/>
    {props.info}
</div>