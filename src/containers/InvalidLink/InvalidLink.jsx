import React from 'react';
import styles from './InvalidLink.scss'
import ContentHeader from 'components/ContentHeader/ContentHeader'
import bgWeb from 'statics/images/web/ResetDesktop.jpg';
import bgMobile from 'statics/images/mobile/ResetMobile.jpg';

const InvalidLink = ()=> [
    <ContentHeader key={'ContentHeader'}
        title={`Oops, something went wrong`}
        bgWeb={bgWeb}
        bgMobile={bgMobile}
    />,
    <div key={'Content'} className={styles.container}>
        <h2>
            {'Sorry, this link is not valid. Please contact us that we can help you: '}
            <a href='mailto:support@everskill.de'>support@everskill.de</a>
        </h2>
    </div>
]
InvalidLink.propTypes = {
}
InvalidLink.defaultProps = {
}
export default InvalidLink