import React from 'react';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
import InvalidLink from './InvalidLink'

it('renders without crashing', () => {
    const props = {}
    const wrapper = Enzyme.shallow(<InvalidLink {...props}/>)
    expect(wrapper!=undefined).toBe(true)
});